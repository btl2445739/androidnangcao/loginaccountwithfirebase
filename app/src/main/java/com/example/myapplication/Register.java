package com.example.myapplication;

import androidx.annotation.NonNull;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Register extends AppCompatActivity {

    FirebaseAuth mAuth;
    EditText name,phoneNumber,address, email,passWord;
    Button registerbtn;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        name= findViewById(R.id.Name);
        phoneNumber= findViewById(R.id.phoneNumber);
        address= findViewById(R.id.Address);
        email=findViewById(R.id.registerEmail);
        passWord=findViewById(R.id.password);
        registerbtn=findViewById(R.id.registerbtn);
        progressBar=findViewById(R.id.progressBar);

        mAuth = FirebaseAuth.getInstance();


//        if(mAuth.getCurrentUser() != null){
//            startActivity(new Intent(getApplicationContext(), SecondActivity.class));
//
//        }
        registerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Email= email.getText().toString().trim();
                String Pass= passWord.getText().toString().trim();
                String PhoneNumber= phoneNumber.getText().toString().trim();
                String Name= name.getText().toString().trim();
                String Address= address.getText().toString().trim();

                if(TextUtils.isEmpty(Email)){
                    email.setError("Nhập Email");
                    return;
                }
                if(TextUtils.isEmpty(Pass)){
                    passWord.setError("Nhập mật khẩu");
                    return;
                }
                if(TextUtils.isEmpty(PhoneNumber)){
                    phoneNumber.setError("Nhập số điện thoại");
                    return;
                }
                if(TextUtils.isEmpty(Name)){
                    name.setError("Nhập tên");
                    return;
                }
                if(TextUtils.isEmpty(Address)){
                    address.setError("Nhập địa chỉ");
                    return;
                }
                if(Pass.length() <6){
                    passWord.setError("Mật khẩu ít nhất 6 ký tự");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
//                Dk len firebasse
                mAuth.createUserWithEmailAndPassword(Email,Pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            FirebaseUser user= mAuth.getCurrentUser();
                            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    showDialog();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    Toast.makeText(Register.this,"Không gửi được email xác thực "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                        else{
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(Register.this,"Lỗi "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }

        });


    }
    public void showDialog(){
        AlertDialog.Builder builder= new AlertDialog.Builder(Register.this);
        builder.setTitle("Đăng ký thành công");
        builder.setMessage("Đã gửi email xác thực. Vui lòng vào địa chỉ email để xác thực tài khoản.Bạn sẽ không đăng nhập được nếu không xác thực tài khoản.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    };
}